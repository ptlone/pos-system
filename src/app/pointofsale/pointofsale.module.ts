// Angular Dependencies
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Router
import {PointOfSaleRoutes} from './pointofsale.routing';

// Core Pages Layout Components
import { SharedModule } from '../@pages/components/shared.module';
import {pgTabsModule} from '../@pages/components/tabs/tabs.module';

// Thirdparty Dependencies - table and model
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ModalModule } from 'ngx-bootstrap';

// Component Imports
import {PosComponent} from './pos/pos.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    pgTabsModule,
    RouterModule.forChild(PointOfSaleRoutes),
    SharedModule,
    NgxDatatableModule,
    ModalModule
  ],
  declarations: [PosComponent]
})
export class PointOfSaleModule {}
