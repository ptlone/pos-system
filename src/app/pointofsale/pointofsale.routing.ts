import { Routes } from '@angular/router';
import {PosComponent} from './pos/pos.component';

export const PointOfSaleRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'pos',
                component: PosComponent,
                data: {
                    title: 'pos'
                }
            },
        ]
    }
];
